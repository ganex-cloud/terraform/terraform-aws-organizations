output "this_organizations_organizational_unit_arn" {
  value       = concat(aws_organizations_organizational_unit.this.*.arn, [""])[0]
  description = "ARN of the organizational unit"
}

output "this_organizations_organizational_unit_id" {
  value       = concat(aws_organizations_organizational_unit.this.*.id, [""])[0]
  description = "Identifier of the organization unit"
}
