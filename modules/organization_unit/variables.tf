variable "name" {
  default     = ""
  description = "The name for the organizational unit"
}

variable "parent_id" {
  default     = ""
  description = "ID of the parent organizational unit, which may be the root"
}
