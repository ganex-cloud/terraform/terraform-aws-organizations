variable "feature_set" {
  default     = "ALL"
  description = "Specify ALL (default) or CONSOLIDATED_BILLING"
}

variable "enable_policy_types" {
  default     = []
  description = "List of Organizations policy types to enable in the Organization Root. Organization must have feature_set set to ALL. For additional information about valid policy types (e.g. SERVICE_CONTROL_POLICY and TAG_POLICY), see the [AWS Organizations API Reference.](https://docs.aws.amazon.com/organizations/latest/APIReference/API_EnablePolicyType.html)"
}

variable "aws_service_access_principals" {
  default     = []
  description = "List of AWS service principal names for which you want to enable integration with your organization. This is typically in the form of a URL, such as service-abbreviation.amazonaws.com. Organization must have feature_set set to ALL. For additional information, see the [AWS Organizations User Guide.](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_integrate_services.html)."
}
