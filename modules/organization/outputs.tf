output "this_organizations_organization_accounts" {
  value       = concat(aws_organizations_organization.this.*.accounts, [""])[0]
  description = "List of organization accounts excluding the master account"
}

output "this_organizations_organization_arn" {
  value       = aws_organizations_organization.this.arn
  description = "ARN of the organization"
}

output "this_organizations_organization_id" {
  value       = aws_organizations_organization.this.id
  description = "ID of the organization"
}

output "this_organizations_organization_master_account_arn" {
  value       = aws_organizations_organization.this.master_account_arn
  description = "ARN of the master account"
}

output "this_organizations_organization_master_account_email" {
  value       = aws_organizations_organization.this.master_account_email
  description = "Email of the master account"
}

output "this_organizations_organization_master_account_id" {
  value       = aws_organizations_organization.this.master_account_id
  description = "ID of the master account"
}

output "this_organizations_organization_non_master_accounts" {
  value       = concat(aws_organizations_organization.this.*.non_master_accounts, [""])[0]
  description = "List of organization accounts excluding the master account"
}

output "this_organizations_organization_roots" {
  value       = concat(aws_organizations_organization.this.*.roots, [""])[0]
  description = "List of organization roots"
}
