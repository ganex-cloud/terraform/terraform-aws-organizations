output "this_organizations_account_arn" {
  value       = concat(aws_organizations_account.this.*.arn, [""])[0]
  description = "ARN of the organization account"
}

output "this_organizations_account_id" {
  value       = concat(aws_organizations_account.this.*.id, [""])[0]
  description = "Identifier of the organization account"
}
